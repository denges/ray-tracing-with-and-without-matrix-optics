# Ray Tracing with and without Matrix Optics

This is a undergraduate photonics course project. The aim is to compare the results from ray tracing calculations obtained from matrix optics(MO) to their exact geometric counterpart. It can nicely be seen when MO performs well and when it does not.

![](example.png)

## Introduction
MO is a linear approximation of ray optics which naturally fails whenever the small angle approximation is not valid. This can be seen in the self-explanatory Jupyter notebook where you can change the optical setup and see what the traced ray looks like. The *red line* is obtained via *matrix optics* and the __green line__ via the __geometric method__. In the documentation the derivation for the exact formulas can be found.

## Installation
Unfortunately, trying this out is not as easy as opening the Jupyter notebook. You will have to install the python package `ipycanvas` first and then enable it in Jupyter. After properly installing the package you can open the notebook like any other and you should be able to see a nice figure of the optical setup with the two rays at the end of the notebook.
