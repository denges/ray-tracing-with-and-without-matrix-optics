\documentclass {scrartcl}
% \documentclass {scrbook}
\usepackage [latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{enumerate}
\usepackage{amsmath, amssymb}
\usepackage{graphicx}
%Fix extension algorithm
\usepackage{grffile}
%table row spacing
\usepackage{booktabs}
\graphicspath{ {images/} }
\usepackage{pgfplots}
\pgfplotsset{}
\usepackage{subfloat}
\usepackage{subcaption}
\usepackage{placeins}
\delimitershortfall-1sp
\newcommand\abs[1]{\left|#1\right|}
\usepackage{gensymb} %\degree
\usepackage{mathtools}
\usepackage{hyperref}
\hypersetup{pdftex}
\usepackage{hypcap}
\usepackage{amsthm}
\newtheorem{lemma}{Lemma}
\usepackage[textsize=small, textwidth=0.8in, disable]{todonotes}
% \usepackage[textsize=small, textwidth=0.8in]{todonotes}
\usetikzlibrary{calc}
\usetikzlibrary{math}
% \usepackage{natbib}
% \usepackage[authoryear]{natbib}
\usepackage{bm}
\usepackage{setspace}
\usepackage{float}
\newcommand\varpm{\mathbin{\vcenter{\hbox{%
  \oalign{\hfil$\scriptstyle+$\hfil\cr
          \noalign{\kern-.3ex}
          $\scriptscriptstyle({-})$\cr}%
}}}}
\newcommand\varmp{\mathbin{\vcenter{\hbox{%
  \oalign{$\scriptstyle({+})$\cr
          \noalign{\kern-.3ex}
          \hfil$\scriptscriptstyle-$\hfil\cr}%
}}}}

\begin{document}
%table row spacing
\renewcommand{\arraystretch}{1.8}
% title page
\title{Ray - Tracing with Matrix Optics - Its Beautiful Simplicity and its Shortcomings}
\date{\today}
\subtitle{Photonics I - Project, Leipzig University}
\author{Denis Gessert}
\maketitle

\tableofcontents
\listoftodos
\todo{todonotes package option auf disable stellen}

\section{Abstract}
Matrix Optics is introduced early on in under-graduate studies and it makes it easy to believe that a ray - tracing problem of a complex optical setup with $n$ components is as easy as multiplying $2n+1$ matrices\footnote{$2n+1 = n + n + 1$ matrices: $n$ for the elements, $n$ for the ray propagation after each element and 1 for the ray propagation onto the first element.}.

Matrix optics uses the small angle approximation, i.e. $\sin \alpha \simeq \tan \alpha \simeq \alpha$. So, naturally it will fail whenever the angle between the ray and the optical axis exceeds the small angle limit, say 5\textdegree.

In this project matrix optics is compared to a geometric approach which is exact within the scope of ray optics. Each optical element was independently analyzed to obtain the individual error margin in dependence of the input parameters. In further studies more complex optical setups could be used to investigate error propagation.% Then, a couple of optical setups were used to get an idea of the error propagation.
\section{Introduction}
Ray transfer matrix analysis is an easy way to perform ray-tracing calculations. A ray in space is described by its distance and angle with respect to the optical axis which is expressed by a vector $( y, ~ \theta )^T$. For any optical element, such as interfaces or lenses, a $2\times 2$ transfer matrix, $\hat A$, can be found that linearly approximates the new ray vector, i.e.
  $$\begin{pmatrix}y'\\\theta'\end{pmatrix}  = \hat A \begin{pmatrix}y\\\theta\end{pmatrix}.$$

Provided an optical setup with $n$ elements, the final distance and angle can be obtained via
$$\begin{pmatrix}y'\\\theta'\end{pmatrix}  = \hat A_n \dots \hat A_1 \begin{pmatrix}y\\\theta\end{pmatrix}.$$
where $\hat A_1$ is the transfer matrix of the first element and $\hat A_n$ the one of the last. Note, that in this notation we count ray propagation as an optical element, whereas we did not earlier. While this is indeed a nice and easy description it can fail when
\begin{itemize}
  \item the small angle approximation is violated,
  \item the distance of the ray from the optical axis is too large (in case of non-linear interfaces),
  \item or when the setup contains of too many elements and the individual (small) errors accumulate to a error margin larger than wanted.
\end{itemize}

In section \ref{sec:theory}, matrix optics is revised and more accurate formulas for ray propagation and refraction derived.

In section \ref{sec:results} both methods are used to perform ray-tracing calculations. It is outlined in which cases an approximation with Matrix Optics is valid and when not.
\section{Theory}
\label{sec:theory}
\subsection*{Wave Propagation}
Within matrix optics wave propagation is described by
$$\begin{pmatrix}y'\\\theta'\end{pmatrix} = \begin{pmatrix}1 & d \\ 0 & 1\end{pmatrix} \begin{pmatrix}y\\\theta\end{pmatrix} = \begin{pmatrix}y+ d \theta \\\theta\end{pmatrix}$$
This approximation is valid for $\tan \theta \simeq \theta$ and accurately wave propagation is described by
$$\begin{pmatrix}y'\\\theta'\end{pmatrix} = \begin{pmatrix}y+ d \tan \theta \\ \theta\end{pmatrix}$$
\subsection*{Refraction at a Flat Interface}
Refraction is described by Snell's Law, i.e.
$$n_1 \sin \alpha = n_2 \sin \beta,$$ where $\alpha$ and $\beta$ are the incoming and outgoing indices and $n_1$ and $n_2$ the initial and final indices of refraction. Replacing $\alpha$ by $\theta$ and $\beta$ by $\theta'$, one obtains
$$\theta' = \sin^{-1}\left(\frac{n_1}{n_2} \sin \theta\right).$$

Within the small angle approximation this simplifies to $\theta' = \frac{n_1}{n_2} \theta$ or in matrix optics terms:

$$\begin{pmatrix}y'\\\theta'\end{pmatrix} = \begin{pmatrix}1 & 0 \\ 0 & \frac{n_1}{n_2}\end{pmatrix} \begin{pmatrix}y\\\theta\end{pmatrix}$$

\subsection*{Refraction at a Curved Interface}
The transfer matrix for refraction at a curved interface is given by 
$$\begin{pmatrix}y'\\\theta'\end{pmatrix} = \begin{pmatrix}1 & 0 \\ \frac{n_1-n_2}{R n_2} & \frac{n_1}{n_2}\end{pmatrix} \begin{pmatrix}y\\\theta\end{pmatrix},$$
where $n_1$ and $n_2$ again are the initial and final indices of refraction and $R$ is the radius of curvature at the interface\footnote{We only consider spherical interfaces and spherical lenses created by such. The line of thought for parabolic or other non-spherical interfaces is the same but out of the scope for this project.}.

The following derivation is a little more involved, but the basic principle is the same as for straight interfaces, namely Snell's Law. %Instead of directly using the angle $\theta$ to apply Snell's Law, we need to construct the plane of refraction, apply Snell's Law locally and then calculate $\theta'$.
Things get a little complicated as the plane of refraction is not perpendicular to the optical axis (unless $\theta = 0$\textdegree).

So, given the curved interface is at position $x$ and our ray is incident with distance $y$ and angle $\theta$ with respect to the optical axis, we need to \begin{enumerate}
  \item determine the point of refraction $(x',y') = (x + \Delta x, y+\Delta y)$,
  \item construct the plane of refraction and obtain the angle between the normal and the optical axis, $\varphi$,
  \item obtain the incident angle, $\alpha$,
  \item use Snell's Law to get the outgoing angle, $\beta$,
  \item and finally determine the angle of the outgoing ray with respect to the optical axis, $\theta'$.
\end{enumerate}
The nomenclature of above steps becomes clear in Figure~\ref{fig:CurvedInterfaceNomenclature}.
\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{CurvedInterfaceNomenclature}
  \caption{Sketch of a curved(spherical) interface.}
  \label{fig:CurvedInterfaceNomenclature}
\end{figure}
First, we obtain the point of refraction, $(x',y')$. We note that the ray propagation happens until this point. Thus, we can use $\theta$ to express $(x',y')$ in terms of $\Delta x$ and $\theta$, i.e.
\begin{align}
  x' &= x + \Delta x \label{equ:XPCurvedInterface} \\
  y' &= y + \Delta x \tan \theta \label{equ:YPCurvedInterface}
\end{align}
The equation for the circle is $$R^2 = y_0^2 + (x_0 - x - R)^2,$$ where any point $(x_0,y_0)$ satisfying the equation lies on the circle. As $(x',y')$ lies on the circle as well, we can combine the equations to 
$$R^2 = \left(y + \Delta x \tan \theta \right)^2 + \left(\Delta x - R\right)^2.$$
This last equation is a quadratic equation in $\Delta x$ and thus can be solved for it, i.e.
\begin{equation}
  \Delta x = \frac{R - y \tan \theta \varmp \sqrt{(R - y \tan \theta)^2 - y^2 (\tan^2 \theta +1)}}{\tan^2 \theta + 1}. \label{equ:DeltaXCurvedInterface}
\end{equation}
From the two solutions($\pm$), the solution with the minus - sign is the physically relevant one, as we are interested at the first intersection with the circle\footnote{This is true when $R>0$. In the case of $R<0$, the + solution is the physically relevant one.}. Plugging this into (\ref{equ:XPCurvedInterface}) and (\ref{equ:YPCurvedInterface}) we can now determine the point of refraction.

Next, we want to calculate $\varphi$. Consider the rectangular triangle given by the points $A(x+R,0)$, $B(x',0)$ and $C(x',y')$. Then, $$\tan \varphi = \frac{y'}{R+x-x'} = \frac{y'}{R-\Delta x}.$$
Hence, we obtain the following equation for the angle between the normal and the optical axis, $\varphi$:
\begin{equation}
  \varphi = \tan^{-1}\left(\frac{y'}{R-\Delta x}\right) \label{equ:PhiCurvedInterface}
\end{equation}

To find $\alpha$ consider the triangle with the points $(x+R,0)$, $(x',y')$ and the point where the ray first crosses the optical axis(where $\theta$ is labelled). The unlabelled angle in said triangle equals $180\textdegree - \alpha$. As the sum of all angles in a triangle equals $180\textdegree$, we have $180\textdegree = \theta + \varphi + 180\textdegree - \alpha.$ Thus, we get for the incident angle the following expression, 
\begin{equation}
  \alpha = \varphi + \theta \label{equ:AlphaCurvedInterface}
\end{equation}

The outgoing angle $\beta$, we can now find via Snell's Law, i.e.
\begin{equation}
  \beta = \sin^{-1} \left( \frac{n_1}{n_2} \sin \alpha \right) .\label{equ:BetaCurvedInterface}
\end{equation}

It only remains to find $\theta'$. It is easy to see that the two orange angles both are $90\textdegree - \varphi$. At $(x',y')$, the orange angle, $-\theta'$\footnote{Be careful: In the sketch $-\theta'$ is drawn, not $\theta'$! This sign occurs because the angle with the optical axis opens in negative direction rather than the positive direction.} and $\beta$ add up to 90 \textdegree. Thus, we can express $\theta'$ as
\begin{equation}
  \theta' = \beta - \varphi. \label{equ:ThetaPCurvedInterface}
\end{equation}
This concludes the geometric derivation of refraction at a curved interface.

Assuming $\Delta x = \Delta y = 0$ and small angle assumption for $\alpha, \beta, \theta, \theta'$ and $\varphi $ we obtain the matrix optics approximation. $y' = y$ as $\Delta y = 0$.
\begin{align*}
  \theta' &\stackrel{(\ref{equ:ThetaPCurvedInterface})}{=} \beta - \varphi \\
  &\stackrel{(\ref{equ:BetaCurvedInterface})}{=} \sin^{-1} \left( \frac{n_1}{n_2} \sin \alpha \right) - \varphi \simeq \frac{n_1}{n_2} \alpha - \varphi\\
  &\stackrel{(\ref{equ:AlphaCurvedInterface})}{=} \frac{n_1}{n_2} \left(\theta+\varphi\right) - \varphi=\frac{n_1 - n_2}{n_2} \varphi + \frac{n_1}{n_2} \theta \\
  &\stackrel{(\ref{equ:PhiCurvedInterface})}{=} \frac{n_1 - n_2}{n_2} \tan^{-1}\left( \frac{y'}{R-\Delta x}\right) + \frac{n_1}{n_2} \theta \simeq \frac{n_1 - n_2}{n_2} \tan^{-1}\left( \frac{y}{R}\right) + \frac{n_1}{n_2} \theta \\
  \theta' &\simeq \frac{n_1 - n_2}{R n_2} y + \frac{n_1}{n_2} \theta,
\end{align*}
which is the same as in the matrix expression.
\subsection*{Lenses}
Spherical lenses are an application of refraction at curved interfaces. Concatenating two interfaces, one with positive radius, one with negative radius makes a lens. Thus, no further calculation is necessary as we can treat a lens as two interfaces separated by space where the ray propagates.

The Lens-Maker's equation relates the radii of curvature to the focal length of the lens, i.e.
\begin{equation}
  {\frac  {1}{f}}={\frac  {n_2-n_{1}}{n_{1}}}\left({\frac  {1}{R_{1}}}-{\frac  {1}{R_{2}}}+{\frac  {(n_2-n_{1})d}{n_2 R_{1}R_{2}}}\right),\label{equ:LensMakerEqu}
\end{equation}
where $n_1$ and $n_2$ are the initial and final index of refraction, $R_1$ and $R_2$ the two radii (and usually have different signs!) and $d$ the distance between the two interfaces.
\section{Results}
\label{sec:results}
In this section results from ray tracing calculations for the optical elements discussed in section \ref{sec:theory} are presented. In each figure the \textit{red line} is obtained via \textit{matrix optics} and the \textbf{green line} via the \textbf{geometric method} discussed in section \ref{sec:theory}.

% \subsection{Individual Optical Elements}
\subsubsection*{Ray Propagation}
Figure~\ref{fig:RayPropagation} shows the ray propagation from a starting point at various angles. Up to an angle of 10\textdegree, there is only little noticeable difference between the two methods. At angles of 15\textdegree and more two clear differing lines can be seen and beyond 30\textdegree matrix optics becomes very erroneous.

As a side note, you can observe that everything including the deviations is symmetric with respect to $\theta = 0$. Further, note that the initial choice of $y=0$ is arbitrary and could as well be chosen differently.
\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{RayPropagation}
%   Y_0 = [0] * 15
% THETA_0 = [35*pi/180, 30*pi/180, 25*pi/180, 20*pi/180, 15*pi/180,10*pi/180,5*pi/180,0,-5*pi/180,-10*pi/180,-15*pi/180, -20*pi/180,-25*pi/180, -30*pi/180, -35*pi/180]
% OBJECTS = [{"type": "propagate", "length": 8}]
%   WIDTH = 1600
%   HEIGHT = 800
%   FONT = '32px arial'
%   MIN_X, MAX_X = 0, 8 # Choose such that HEIGHT / WIDTH = (MAX_Y-MIN_Y) / (MAX_X-MIN_X)
%   MIN_Y, MAX_Y = -2, 2
  \caption{Ray propagation originating from $y=0$ at various angles $\theta$. ${\theta = -35}$\textdegree$, -30$\textdegree$, -25$\textdegree$, \dots, 0$\textdegree$, 5$\textdegree$, \dots, 35$\textdegree}
  \label{fig:RayPropagation}
\end{figure}
\FloatBarrier
\subsubsection*{Refraction at a Straight Interface}
The matrix optics approximation at a straight interface relies on $\theta$ and $\theta'$ satisfying the small angle condition. For an interface from a less dense medium to a denser one(see Figure~\ref{fig:RefractionStraightInterfaceToDense}), we only need to worry about $\theta$ being small, as $\theta' < \theta$.

At a interface to a less dense medium, we have to be a little careful, as comparatively small angles $\theta$ can produce rather large angles $\theta'$. This can be seen quite well in Figure~\ref{fig:RefractionStraightInterfaceFromDense}. Already for $\theta = 10$\textdegree, the deviation after the interface are quite large and near the angle of total reflection, matrix optics full on fails.
\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{RefractionStraightInterfaceToDense}
% Y_0 = [0] * 11
% THETA_0 = [24.6*pi/180, 20*pi/180, 15*pi/180,10*pi/180,5*pi/180,0,-5*pi/180,-10*pi/180,-15*pi/180, -20*pi/180, -24.6*pi/180]
% OBJECTS = [{"type": "propagate", "length": 2}, {"type": "interface", "n": 2.4}, {"type": "propagate", "length": 6}]
% n_OUTSIDE = 1
  \caption{Refraction at a straight interface originating from $y=0$ at various angles $\theta$. The interface is from vacuum($n=1$) to diamond($n\simeq 2.4$). ${\theta = -24.6}$\textdegree$, -20$\textdegree$, -15$\textdegree$, \dots, 0$\textdegree$, 5$\textdegree$, 10$\textdegree$, \dots, 20 $\textdegree$, 24.6$\textdegree}
  \label{fig:RefractionStraightInterfaceToDense}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{RefractionStraightInterfaceFromDense}
% Y_0 = [0] * 11
% THETA_0 = [24.6*pi/180, 20*pi/180, 15*pi/180,10*pi/180,5*pi/180,0,-5*pi/180,-10*pi/180,-15*pi/180, -20*pi/180, -24.6*pi/180]
% OBJECTS = [{"type": "propagate", "length": 2}, {"type": "interface", "n": 1}, {"type": "propagate", "length": 6}]
% n_OUTSIDE = 2.4
  \caption{Refraction at a straight interface originating from $y=0$ at various angles $\theta$. The interface is from diamond($n\simeq 2.4$) to vacuum($n=1$). ${\theta = -24.6}$\textdegree$, -20$\textdegree$, -15$\textdegree$, \dots, 0$\textdegree$, 5$\textdegree$, 10$\textdegree$, \dots, 20 $\textdegree$, 24.6$\textdegree. The angle of total reflection at this interface is $\theta_\text{tot} = \sin^{-1}(\frac{1}{2.4}) \approx 24.6243$\textdegree.}
  \label{fig:RefractionStraightInterfaceFromDense}
\end{figure}
\FloatBarrier
\subsubsection*{Refraction at a Curved Interface}
At curved interfaces again, we can see that matrix optics fails when the small angle approximation is not satisfied. Additionally, as can been seen in Figure~\ref{fig:RefractionCurvedInterfaceToDenseParallel} the rays have to be sufficiently close to the optical axis otherwise matrix optics will also fail.
\begin{figure}[h]
  %Y_0 = [0] * 13
  % THETA_0 = [30*pi/180, 25*pi/180, 20*pi/180, 15*pi/180,10*pi/180,5*pi/180,0,-5*pi/180,-10*pi/180,-15*pi/180, -20*pi/180,-25*pi/180, -30*pi/180]
  % OBJECTS = [{"type": "propagate", "length": 1.5},{"type": "curvedInterface", "n": 1.5, "R": 2},{"type": "propagate", "length": 6.5}]
  \centering
  \includegraphics[width=0.8\textwidth]{RefractionCurvedInterfaceToDensey0}
  \caption{Refraction at a curved interface originating from $y=0$ at various angles $\theta$. The interface is from vacuum($n=1$) to glass($n\simeq 1.5$). ${\theta = -30}$\textdegree$, -25$\textdegree$, -20$\textdegree$, \dots, 0$\textdegree$, 5$\textdegree$, 10$\textdegree$, \dots, 25 $\textdegree$, 30$\textdegree.}
  \label{fig:RefractionCurvedInterfaceToDensey0}
\end{figure}
\begin{figure}[h]
  % Y_0 = [1.75,1.5,1.25,1,0.75,0.5,0.25,0,-0.25,-0.5,-0.75,-1,-1.25,-1.5,-1.75]
  % THETA_0 = [0] * len(Y_0)
  % OBJECTS = [{"type": "propagate", "length": 1.5},{"type": "curvedInterface", "n": 1.5, "R": 2},{"type": "propagate", "length": 6.5}]
\centering
\includegraphics[width=0.8\textwidth]{RefractionCurvedInterfaceToDenseParallel}
\caption{Refraction of parallel light ($\theta = 0$) at a curved interface. The interface is from vacuum($n=1$) to glass($n\simeq 1.5$).}
\label{fig:RefractionCurvedInterfaceToDenseParallel}
\end{figure}
Note, that in Figure \ref{fig:RefractionCurvedInterfaceToDenseParallel} the rays as calculated by matrix optics meet at the focal point, whereas they clearly do not for rays far from the optical axis as the geometrical calculations show. This effect is called \emph{spherical aberration}. This effect we will also see in the lens section.
\FloatBarrier
% \subsection{Setups Containing Multiple Optical Elements}
\subsubsection*{Lenses}
Table~\ref{tab:lenses} shows the results of the ray tracing calculations for lenses using the two methods. Parallel rays going through a convex lens become focal rays and vice versa. This behavior is reproduced both by matrix optics and by geometric optics. In the case of geometric ray tracing, however, light that is far from the optical axis behaves anomalously. This is due to \emph{spherical aberration}, an effect not captured by matrix optics.

To create focal rays, the focal length $f$ needs to be calculated which can be done by using the Lensmaker's formula, equation~(\ref{equ:LensMakerEqu}). This produces quite parallel light with Geometric Ray Tracing, which is clearly not the case with matrix optics. This comes by surprise as the parallel light is clearly focussed onto the focal point.

After calculating the zero intercept observed when applying parallel light in matrix optics, it appears that matrix optics produces a slightly different focal point than would be expected. When this focal length is used to produce focal rays, in the case of matrix optics these become again clearly parallel whereas geometric ray tracing shows that this should not be the case.

In the example shown in Table~\ref{tab:lenses} one finds a focal length of $f=\frac{48}{17}\approx 2.82$ via matrix optics which deviates from $f=\frac{18}{7}\approx 2.57$, which is found with the Lensmaker equation.
\begin{table}
  \centering
  \begin{tabular}{p{2cm}|p{2.5cm}|c|c}
    \multicolumn{1}{c|}{Name} & \multicolumn{1}{|c|}{Notes} & Matrix Optics Ray Tracing & Geometric Ray Tracing \\ \hline
%     Y_0 = [1.5, 1.25,1,0.75,0.5,0.25,0,-0.25,-0.5,-0.75,-1,-1.25, -1.5] #[0] * 11 # [3.5,-3.5] 
% THETA_0 = [0] * len(Y_0) #[25*pi/180,20*pi/180, 15*pi/180,10*pi/180,5*pi/180,0,-5*pi/180,-10*pi/180,-15*pi/180,-20*pi/180,-25*pi/180] # [-0.5,0.5]
% OBJECTS = [{"type": "propagate", "length": 2.823529411764706},{"type": "curvedInterface", "n": 1.5, "R": 3},
%            {"type": "propagate", "length": 1},{"type": "curvedInterface", "n": 1, "R": -3},{"type": "propagate", "length": 6}]
% #2.5714285714285716 = 18/7
% 2.823529411764706 = 48/17
% 
    Convex lens $\quad\quad$\footnotesize($R_1 = 3, R_2 = -3, d=1$) & \footnotesize parallel input light& \raisebox{-0.8\totalheight}{\includegraphics[width=0.38\textwidth]{ConvexLensParallel_mat}}& \raisebox{-0.8\totalheight}{\includegraphics[width=0.38\textwidth]{ConvexLensParallel_geo}} \\
    & \footnotesize focal rays as input (focal point determined with equation(\ref{equ:LensMakerEqu}), ${f\approx 2.6}$) & \raisebox{-0.8\totalheight}{\includegraphics[width=0.38\textwidth]{ConvexLensFromGeoFocalPoint_mat}}& \raisebox{-0.8\totalheight}{\includegraphics[width=0.38\textwidth]{ConvexLensFromGeoFocalPoint_geo}} \\
    & \footnotesize focal rays as input (focal point determined with matrix optics, ${f\approx 2.8}$) & \raisebox{-0.8\totalheight}{\includegraphics[width=0.38\textwidth]{ConvexLensFromMatFocalPoint_mat}}& \raisebox{-0.8\totalheight}{\includegraphics[width=0.38\textwidth]{ConvexLensFromMatFocalPoint_geo}} \\ \hline 
    Concave lens $\quad\quad$\footnotesize(${R_1 = -3}, R_2 = 3, d=1$) & parallel input light & \multicolumn{2}{|c}{\raisebox{-0.8\totalheight}{\includegraphics[width=0.76\textwidth]{ConcaveLensParallel}}}
  \end{tabular}
  \caption{Ray Tracing for convex and concave lenses.}
  \label{tab:lenses}
\end{table}
\FloatBarrier
\section{Conclusion and Outlook}
Matrix optics is a simple and fast way to perform ray tracing calculations. As only $2\times 2$ matrices have to be multiplied it is still feasible to do these kind of calculations on paper where exact geometric calculations would be too tedious. Nonetheless, most applications of matrix optics are performed on a computer rather than on paper. In this case, there is no good reason to use matrix optics as in most scenarios a computer can do the exact calculation in a similar time as the approximative calculation.

Matrix optics performs quite well, as long as all considered angles are not much larger than 5\textdegree. For curved interfaces additionally the distance from the optical axis should be much smaller than the radius of curvature.

Furthermore, it was found that effects like total reflection as well as lens aberration can be seen with the geometric approach whereas these are approximated away by matrix optics. No assumptions were made other than that Snell's Law is true.

As an outlook more interfaces could be implemented. This includes for example straight interfaces that are not perpendicular to the optical axis or parabolic curved interfaces. The first can be used to create a prism, the latter to create a parabolic lens. Reflection was not discussed at all, but could of course also be implemented. While qualitatively the deviation resulting from the matrix optics approximation was discussed in detail, it remains to quantitatively analyze it as a function of input angle and distance from the optical axis. This could give further insight of when and when not to use matrix optics.

%\bibliography{references}{}
%\bibliographystyle{plain}
%\bibliographystyle{h-physref}

\end{document}
